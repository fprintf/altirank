# altirank

Altirank is a service to run in conjunction with AltitudeGAme servers.

Altirank supports multiple servers in one or more log files as defined by the altirank.conf file.

# Installation

Currently installation is just copying altirank and altirank.conf to the destination server
