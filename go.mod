module github.com/fprintf/altirank

go 1.13

require (
	github.com/btcsuite/btcd v0.20.1-beta
	github.com/hpcloud/tail v1.0.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/nxadm/tail v1.4.4
	golang.org/x/sys v0.0.0-20200728102440-3e129f6d46b1 // indirect
)
