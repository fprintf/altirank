package main

import (
	"log"
	"math"
)

type PlayerStats struct {
	Wins   int
	Losses int
	Rating float64
}

var (
	GameModeBall = "ball"
	GameModeTdm  = "tdm"
	GameMode1dm  = "1dm"
)

type Player struct {
	VaporId string                  `json:"vaporId"`
	Nick    string                  `json:"nickname"`
	Ip      string                  `json:"ip"`
	Team    int                     `json:"team"`
	Id      int                     `json:"player"`
	Wins    int                     `json:"-" sql:"wins"`
	Losses  int                     `json:"-" sql:"losses"`
	Rating  float64                 `json:"-" sql:"rating"`
	Stats   map[string]*PlayerStats `json:"-"`
}

func (p *Player) IsBot() bool {
	return p.VaporId == "00000000-0000-0000-0000-000000000000"
}

func (p *Player) Ratio() float64 {
	return float64(p.Wins+1) / float64(p.Losses+1)
}

func (p *Player) UpdateRating(enemyRating float64, won int) float64 {
	// Calculate the constant K by the number of games they have played
	// less games played higher K value, more games played smaller K value
	games := p.Wins + p.Losses + 1
	k := int(1.0/float64(games)*32 + 3)

	WinProb := 1.0 * 1.0 / (1 + 1.0*math.Pow(10, 1.0*(enemyRating-p.Rating)/400))
	diff := float64(k) * (float64(won) - WinProb)
	log.Printf("Updating player %s rating old=%v new=%v diff=%v", p.Nick, p.Rating, p.Rating+diff, diff)
	p.Rating += diff
	return p.Rating
}
