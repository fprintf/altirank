// AltitudeServerConfig is used to read and manipulate the altitude
// server configuration
package main

import (
	"encoding/xml"
	"fmt"
	"os"
)

// AltitudeServerConfig defines the altitude servers config
type AltitudeLauncherConfig struct {
	XMLName     xml.Name `xml:"ServerLauncherConfig"`
	Ip          string   `xml:"ip,attr"`
	UpnpEnabled string   `xml:"upnpEnabled,attr"`
	UpdatePort  string   `xml:"updatePort,attr"`
	Servers     struct {
		AltitudeServerConfig []AltitudeServerConfig `xml:"AltitudeServerConfig"`
	} `xml:"servers"`
}
type AltitudeServerConfig struct {
	Port                          string `xml:"port,attr"`
	DownloadMaxKilobytesPerSecond string `xml:"downloadMaxKilobytesPerSecond,attr"`
	DownloadHttpSource            string `xml:"downloadHttpSource,attr"`
	ServerName                    string `xml:"serverName,attr"`
	MaxPlayerCount                string `xml:"maxPlayerCount,attr"`
	Hardcore                      string `xml:"hardcore,attr"`
	AutoBalanceTeams              string `xml:"autoBalanceTeams,attr"`
	PreventTeamSwitching          string `xml:"preventTeamSwitching,attr"`
	DisableBalanceTeamsPopup      string `xml:"disableBalanceTeamsPopup,attr"`
	LanServer                     string `xml:"lanServer,attr"`
	CallEndOfRoundVote            string `xml:"callEndOfRoundVote,attr"`
	DisallowDemoUsers             string `xml:"disallowDemoUsers,attr"`
	RconEnabled                   string `xml:"rconEnabled,attr"`
	RconPassword                  string `xml:"rconPassword,attr"`
	MaxPing                       string `xml:"maxPing,attr"`
	MinLevel                      string `xml:"minLevel,attr"`
	MaxLevel                      string `xml:"maxLevel,attr"`
	SecretCode                    string `xml:"secretCode,attr"`
	CameraViewScalePercent        string `xml:"cameraViewScalePercent,attr"`
	AdminsByVaporID               struct {
		UUID []struct {
			UUID string `xml:"UUID,attr"`
		} `xml:"UUID"`
	} `xml:"adminsByVaporID"`
	MapList struct {
		String []struct {
			Value string `xml:"value,attr"`
		} `xml:"String"`
	} `xml:"mapList"`
	MapRotationList struct {
		String []struct {
			Value string `xml:"value,attr"`
		} `xml:"String"`
	} `xml:"mapRotationList"`
	BotConfig struct {
		NumberOfBots         string `xml:"numberOfBots,attr"`
		BotDifficulty        string `xml:"botDifficulty,attr"`
		BotsBalanceTeams     string `xml:"botsBalanceTeams,attr"`
		BotSpectateThreshold string `xml:"botSpectateThreshold,attr"`
	} `xml:"BotConfig"`
	DisallowedPlaneRandomTypes string `xml:"disallowedPlaneRandomTypes"`
	FreeForAllGameMode         struct {
		ScoreLimit        string `xml:"scoreLimit,attr"`
		RoundLimit        string `xml:"RoundLimit,attr"`
		RoundTimeSeconds  string `xml:"roundTimeSeconds,attr"`
		WarmupTimeSeconds string `xml:"warmupTimeSeconds,attr"`
	} `xml:"FreeForAllGameMode"`
	BaseDestroyGameMode struct {
		RoundLimit        string `xml:"RoundLimit,attr"`
		RoundTimeSeconds  string `xml:"roundTimeSeconds,attr"`
		WarmupTimeSeconds string `xml:"warmupTimeSeconds,attr"`
	} `xml:"BaseDestroyGameMode"`
	ObjectiveGameMode struct {
		GamesPerRound          string `xml:"gamesPerRound,attr"`
		GamesPerSwitchSides    string `xml:"gamesPerSwitchSides,attr"`
		GameWinMargin          string `xml:"gameWinMargin,attr"`
		BetweenGameTimeSeconds string `xml:"betweenGameTimeSeconds,attr"`
		RoundLimit             string `xml:"RoundLimit,attr"`
		RoundTimeSeconds       string `xml:"roundTimeSeconds,attr"`
		WarmupTimeSeconds      string `xml:"warmupTimeSeconds,attr"`
	} `xml:"ObjectiveGameMode"`
	PlaneBallGameMode struct {
		GoalsPerRound     string `xml:"goalsPerRound,attr"`
		RoundLimit        string `xml:"RoundLimit,attr"`
		RoundTimeSeconds  string `xml:"roundTimeSeconds,attr"`
		WarmupTimeSeconds string `xml:"warmupTimeSeconds,attr"`
	} `xml:"PlaneBallGameMode"`
	TeamDeathmatchGameMode struct {
		ScoreLimit        string `xml:"scoreLimit,attr"`
		RoundLimit        string `xml:"RoundLimit,attr"`
		RoundTimeSeconds  string `xml:"roundTimeSeconds,attr"`
		WarmupTimeSeconds string `xml:"warmupTimeSeconds,attr"`
	} `xml:"TeamDeathmatchGameMode"`
	CustomCommands            string `xml:"customCommands"`
	ConsoleCommandPermissions struct {
		AltitudeServerConsoleCommandPermissions []struct {
			ConsoleCommand string `xml:"ConsoleCommand,attr"`
			AllowedGroups  struct {
				AltitudeConsoleGroup []struct {
					Group string `xml:"Group,attr"`
				} `xml:"AltitudeConsoleGroup"`
			} `xml:"AllowedGroups"`
		} `xml:"AltitudeServerConsoleCommandPermissions"`
	} `xml:"consoleCommandPermissions"`
}

func (ac *AltitudeLauncherConfig) LoadFile(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return fmt.Errorf("xml failed to open %v: %w", file, err)
	}
	defer f.Close()

	tmp := &AltitudeLauncherConfig{}
	decoder := xml.NewDecoder(f)
	err = decoder.Decode(tmp)
	if err != nil {
		return fmt.Errorf("xml failed to parse %v: %w", file, err)
	}

	// Copy the contents
	*ac = *tmp

	return nil
}
