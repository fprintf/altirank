package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
)

type AltiEvent struct {
	Port    int         `json:"port"`
	Time    int         `json:"time"`
	Type    string      `json:"type"`
	Player  int         `json:"-"`
	Text    string      `json:"-"`
	Trailer *Trailer    `json:"-"`
	Server  *AltiServer `json:"-"`
	data    map[string]interface{}
}

type AltiEventFunc func(*AltiEvent) error

func NewAltiEvent(trailer *Trailer, line string) (*AltiEvent, error) {
	e := &AltiEvent{Text: line, Trailer: trailer, Player: -1}
	err := json.Unmarshal([]byte(line), e)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal to event: %w", err)
	}

	err = json.Unmarshal([]byte(line), &e.data)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal to generic object: %w", err)
	}
	tmp, _ := e.data["player"].(float64)
	e.Player = int(tmp)

	err = e.UpdateTracking()
	if err != nil {
		return nil, err
	}

	return e, nil
}

func (e *AltiEvent) UpdateTracking() error {
	// Load server into the event
	e.Server = AltiServers.NewAltiServer(e.Port)

	// Update Server player/client tracking state
	switch e.Type {
	case "serverStart":
		e.Server.Reset()
	case "teamChange":
		// -1 is spec, default to spec
		player, ok := e.Server.Players[e.Player]
		if !ok {
			return fmt.Errorf("Missing player data, we have no player at index %d", e.Player)
		}

		team := e.GetInt("team")
		player.Team = -1
		if team == e.Server.Teams[0] {
			player.Team = 0
			log.Printf("Player %d (e.Player %d) nickname %s changed to the left team", player.Id, e.Player, player.Nick)
		} else if team == e.Server.Teams[1] {
			player.Team = 1
			log.Printf("Player %d (e.Player %d) nickname %s changed to the right team", player.Id, e.Player, player.Nick)
		} else {
			log.Printf("Player %d (e.Player %d) nickname %s changed to spectate", player.Id, e.Player, player.Nick)
		}
	case "clientAdd":
		player := &Player{Team: -1}
		err := json.Unmarshal([]byte(e.Text), player)
		if err != nil {
			return fmt.Errorf("failed to parse as clientAdd: %v: %w", e.Text, err)
		}
		log.Printf("Adding client server %v nickname %v client %v player %d", e.Port, player.Nick, player.VaporId, player.Id)
		e.Server.AddPlayer(player.Id, player)
	case "clientRemove":
		e.Server.RemovePlayer(e.Player)
		log.Printf("Removed client server %v player %d nickname: %s", e.Port, e.Player, e.GetString("nickname"))
	case "mapChange":
		server := e.Server
		server.Teams[0] = e.GetInt("leftTeam")
		server.Teams[1] = e.GetInt("rightTeam")
		server.GameMode = e.GetString("mode")
		server.Map = e.GetString("map")
		log.Printf("Server %v map change: %+v", server.Port, server)
	case "consoleCommandExecute":
		// Get player ID by looking up the source vaporId
		// this is because the consoleCommandExecute() event does NOT contain the player number, unfortunately
		e.Player = e.Server.GetPlayerVaporId(e.GetString("source"))
	default:
		// No special state tracking needed
	}

	return nil
}

// Route routes the event for any custom handlers
// TODO now that state has been moved to UpdateTracking()
// TODO this should now use a global eventTypes that lets you add/remove
// TODO events handlers to it (more than one per event) using arrays
// TODO still running each one in its own Go routine
func (e *AltiEvent) Route() error {
	eventTypes := map[string]AltiEventFunc{
		"mapChange":    MapChange,
		"roundEnd":     RoundEnd,
		"clientAdd":    ClientAdd,
		"clientRemove": ClientRemove,
		// TODO: fix PingSummary to properly parse the result, its using an array or something improperly
		//		"pingSummary":           PingSummary,
		"teamChange":            TeamChange,
		"spawn":                 Spawn,
		"consoleCommandExecute": ConsoleCommandExecute,
	}

	proc, ok := eventTypes[e.Type]
	if !ok {
		return fmt.Errorf("unknown event type %v: %+v", e.Type, e)
	}

	go func(e *AltiEvent) {
		err := proc(e)
		if err != nil {
			log.Printf("Error running [%v] event proc: %v", e.Type, err)
		}
	}(e)
	return nil
}

func (e *AltiEvent) GetString(key string) string {
	val, ok := e.data[key].(string)
	if !ok {
		log.Printf("value %v is of type %T not string", e.data[key], e.data[key])
	}
	return val
}

func (e *AltiEvent) GetInt(key string) int {
	val, ok := e.data[key].(float64)
	if !ok {
		log.Printf("value %v is of type %T not int", e.data[key], e.data[key])
	}
	return int(val)
}

func (e *AltiEvent) GetRaw(key string) interface{} {
	return e.data[key]
}

/********************************************************************
* Event handlers
********************************************************************/
func ClientRemove(e *AltiEvent) error {
	return nil
}

func ClientAdd(e *AltiEvent) error {
	e.Server.Whisper(e.Player, "Welcome to ladder. Please see rules at planeball.com")
	e.Server.Whisper(e.Player, "To start a match, join a team and type /y")
	e.Server.Whisper(e.Player, "To see your stats, run the /stats command")
	return nil
}

func PingSummary(e *AltiEvent) error {
	server := AltiServers.GetServer(e.Port)
	if server == nil {
		return fmt.Errorf("PingSummary(): invalid server: %v", e.Port)
	}

	pings := struct {
		ByPlayer map[int]*int `json:"pingByPlayer"`
	}{}

	err := json.Unmarshal([]byte(e.Text), &pings)
	if err != nil {
		return fmt.Errorf("PingSummary(): failed to parse json: %v", err)
	}

	// Do a sanity check to see if we have a valid player list in our state
	// if we have the right number of players, disregard it
	if len(server.Players) == len(pings.ByPlayer) {
		return nil
	}

	for id, _ := range server.Players {
		if pings.ByPlayer[id] != nil {
			continue
		}
		server.RemovePlayer(id)
		log.Printf("Removed invalid client server %v player %d missing from pingSummary", e.Port, id)
	}
	return nil
}

func Spawn(e *AltiEvent) error {
	// {"port":27277,"time":67792,"plane":"Loopy","player":1,"perkRed":"Acid Bomb","perkGreen":"Heavy Armor","team":9,"type":"spawn","perkBlue":"Turbocharger","skin":"No Skin"}
	spawn := struct {
		Plane     string
		PerkRed   string
		PerkGreen string
		PerkBlue  string
		Skin      string
		Player    int
		Team      int
	}{}
	err := json.Unmarshal([]byte(e.Text), &spawn)
	if err != nil {
		return err
	}

	log.Printf("Player spawned %+v", spawn)
	return nil
}

func TeamChange(e *AltiEvent) error {
	server := e.Server
	player, ok := server.Players[e.Player]
	if !ok {
		return fmt.Errorf("Missing player data, we have no player at index %d", e.Player)
	}

	// Check to see if we have teamSize players (teamSize*2 if balance teamType)
	count := server.TotalPlayersActive()
	teamSize := Conf.TeamSize
	teamType := Conf.TeamType
	if tmp, ok := Conf.Servers[server.Port]; ok {
		teamSize = tmp.TeamSize
		teamType = tmp.TeamType
	}
	// Teams have teamSize players on each team now
	// Prevent more than TeamSize players from joining teams
	switch teamType {
	default:
		fallthrough
	case "balance", "balanced":
		if count == teamSize*2 {
		} else if count > teamSize*2 {
			// We have more than teamSize players, reassign this player to spectate
			server.SendCmd("assignTeam %v -1", player.Nick)
		} else {
			// Teams don't have 4v4
			// 1. If startTournament is not currently active, deactivate any current votes
			// 2. Wait for the next teamChange event
		}
	}
	return nil
}

type RoundStats struct {
	Participants []int `json:"participants"`
	Stats        struct {
		Possession     []int `json:"Ball Possession Time"`
		Goals          []int `json:"Goals Scored"`
		Crashes        []int `json:"Crashes"`
		Experience     []int `json:"Experience"`
		Kills          []int `json:"Kills"`
		DamageReceived []int `json:"Damage Received"`
		DamageDealt    []int `json:"Damage Dealt"`
		Assists        []int `json:"Assists"`
		Deaths         []int `json:"Deaths"`
		Multikill      []int `json:"Multikill"`
		KillStreak     []int `json:"Kill Streak"`
	} `json:"participantStatsByName"`
	VaporIds []string `json:"vaporIds,omitempty"`
}

func RoundEnd(e *AltiEvent) error {
	// {"port":27276,"time":9690582,"participantStatsByName":{"Ball Possession Time":[21],"Goals Scored":[3],"Crashes":[1],"Experience":[0],"Kills":[0],"Damage Received":[100],"Longest Life":[28],"Goals Assisted":[0],"Damage Dealt to Enemy Buildings":[0],"Assists":[0],"Deaths":[1],"Multikill":[0],"Damage Dealt":[0],"Kill Streak":[0]},"winnerByAward":{"Most Goal Assists":0,"Best Multikill":0,"Longest Life":0,"Best Kill Streak":0,"Most Deadly":0},"type":"roundEnd","participants":[0]}
	// Count winners and losers based on which team won
	server := e.Server
	stats := &RoundStats{}

	err := json.Unmarshal([]byte(e.Text), &stats)
	if err != nil {
		return err
	}

	// End any active tournament
	server.SendCmd("stopTournament")

	sums := [2]int{0, 0}
	for index, id := range stats.Participants {
		if server.Players[id] == nil {
			continue
		}
		// Add VaporId so the roundStats make sense out of context
		stats.VaporIds = append(stats.VaporIds, server.Players[id].VaporId)

		goals := stats.Stats.Goals[index]
		team := server.Players[id].Team
		if team == -1 {
			continue
		}
		sums[team] += goals
	}
	winningTeam := -1
	losingTeam := -1
	if sums[1] > sums[0] {
		winningTeam = 1
		losingTeam = 0
	} else if sums[0] > 0 {
		winningTeam = 0
		losingTeam = 1
	} else {
		// No winning team decided, so don't increase any scores
		return nil
	}

	// Announce the winners
	teamName := "Left"
	if winningTeam == 1 {
		teamName = "Right"
	}
	server.Message("%s team wins!", teamName)

	// Get average rating for each team
	winTeamAverage := server.TeamRatingAverage(winningTeam, server.GameMode, Conf.RatingMinGames)
	loseTeamAverage := server.TeamRatingAverage(losingTeam, server.GameMode, Conf.RatingMinGames)

	// If noone had any ratings on the team just use the default of 1500
	if winTeamAverage < Conf.RatingDefault {
		winTeamAverage = Conf.RatingDefault
	}
	if loseTeamAverage < Conf.RatingDefault {
		loseTeamAverage = Conf.RatingDefault
	}

	for _, player := range server.Players {
		if player.Team == -1 || player.IsBot() {
			continue
		}

		player, err := Store.GetPlayer(player)
		if err != nil {
			return err
		}

		// Update the stats for recording
		if player.Stats[server.GameMode] == nil {
			player.Stats[server.GameMode] = &PlayerStats{Rating: Conf.RatingDefault}
		}
		player.Wins = player.Stats[server.GameMode].Wins
		player.Losses = player.Stats[server.GameMode].Losses
		player.Rating = player.Stats[server.GameMode].Rating
		if player.Team == winningTeam {
			server.Whisper(player.Id, "You won! Nice job! Enemy team average: %.2f", loseTeamAverage)
			player.UpdateRating(loseTeamAverage, 1)
			player.Wins++
		} else {
			server.Whisper(player.Id, "You lost! The other team probably cheated! Enemy team average: %.2f", winTeamAverage)
			player.UpdateRating(winTeamAverage, 0)
			player.Losses++
		}

		err = Store.UpdatePlayer(player)
		log.Printf("Player %#v getting updated", player)

		server.Whisper(player.Id, "New %s rating: w-l %v-%v (%.2f:1) rating (%.2f)",
			server.GameMode,
			player.Wins,
			player.Losses,
			player.Ratio(),
			player.Rating,
		)
		if err != nil {
			log.Println(fmt.Errorf("failed to update player %v: %w", player, err))
		}

		err = Store.UpdatePlayerStats(player, server.GameMode)
		if err != nil {
			log.Println(fmt.Errorf("failed to update player stats %v gamemode: %v: %w", player, server.GameMode, err))
		}
	}

	output, _ := json.Marshal(stats)
	log.Printf("json stats: mode: %v %v", strings.ToLower(server.GameMode), string(output))
	err = Store.RecordRound(strings.ToLower(server.GameMode), string(output))
	return err
}

func MapChange(e *AltiEvent) error {
	return nil
}

func ConsoleCommandExecute(e *AltiEvent) error {
	//{"port":27276,"time":148997,"arguments":["list","-1"],"source":"11b238a9-78d2-4491-ac82-a7e9d2425fb0","command":"assignTeam","group":"Administrator","type":"consoleCommandExecute"}
	// Source - the VaporId of the person who ran the command
	// Group - Administrator/user etc..
	// Command - the command being sent
	// Args - arguments to the command
	consoleCommand := struct {
		Source  string   `json:"source"`
		Group   string   `json:"group"`
		Command string   `json:"command"`
		Args    []string `json:"arguments"`
	}{}

	err := json.Unmarshal([]byte(e.Text), &consoleCommand)
	if err != nil {
		return fmt.Errorf("failed to parse consoleCommandExecute: %+v", err)
	}

	command := CommandHooks[consoleCommand.Command]
	if command != nil {
		go command(e, e.Server)
	}

	log.Printf("consoleCommandExecute %+v", consoleCommand)
	return nil
}
