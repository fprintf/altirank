package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

type AltiServer struct {
	*ConfigServer
	Port     int
	Map      string
	GameMode string
	Players  map[int]*Player
	// 0 - left team, 1 - right team
	Teams [2]int
}

type AltiServerMap struct {
	*sync.RWMutex
	Servers map[int]*AltiServer
}

func (am *AltiServerMap) NewAltiServer(port int) *AltiServer {
	am.Lock()
	defer am.Unlock()

	server, ok := am.Servers[port]
	if !ok {
		server = &AltiServer{Port: port, Players: map[int]*Player{}, ConfigServer: &ConfigServer{}}
		am.Servers[port] = server
	}

	// Conf is a global variable (in main.go)
	server.TeamSize = Conf.TeamSize
	server.TeamType = Conf.TeamType
	conf, ok := Conf.Servers[port]
	if ok {
		server.ConfigServer = conf
	}

	return server
}

func (am *AltiServerMap) GetServer(port int) *AltiServer {
	return am.Servers[port]
}

func (as *AltiServer) AddPlayer(id int, player *Player) {
	as.Players[id] = player
}

func (as *AltiServer) RemovePlayer(id int) *Player {
	player := as.Players[id]
	delete(as.Players, id)
	return player
}

// Reset resets/removes all players from the server state
func (as *AltiServer) Reset() {
	as.Players = map[int]*Player{}
}

// TotalPlayers retursn the number of players on the server
func (as *AltiServer) GetPlayerVaporId(vaporId string) int {
	for id, player := range as.Players {
		if player.VaporId == vaporId {
			return id
		}
	}
	return -1
}

// TotalPlayers returns the number of players on the server
func (as *AltiServer) TotalPlayers() int {
	return len(as.Players)
}

// TotalPlayersActive returns the number of players actually on a team
func (as *AltiServer) TotalPlayersActive() int {
	total := 0
	for _, player := range as.Players {
		if player.Team != -1 {
			total++
		}
	}
	return total
}

// TeamRatingAverage returns the number of players on the server
// mode - the game mode e.g: ball, tdm, etc..
// minGames - the minimum number of games in that mode required to count the rating
// of a player
// average will be counted for all players _with_ stats and the minGames
// played within that game mode
func (as *AltiServer) TeamRatingAverage(teamid int, mode string, minGames int) float64 {
	rating := float64(0)
	count := 0
	for _, player := range as.Players {
		if player.Team != teamid {
			continue
		}
		player, err := Store.GetPlayer(player)
		if err != nil {
			continue
		}

		// No stats for this player, ignore it
		stats := player.Stats[mode]
		if stats == nil {
			continue
		}

		// Player doesn't have enough games yet to have a decent rating
		games := stats.Wins + stats.Losses + 1
		if games < minGames {
			continue
		}

		rating += stats.Rating
		count++
	}

	return (rating + 1) / float64(count+1)
}

// SendCmd sends a command to the altitude server with printf style semantics
func (as *AltiServer) SendCmd(cmd string, args ...interface{}) {
	quote := func(arg string) string {
		ret := strings.ReplaceAll(arg, "\"", "\\\"")
		ret = strings.ReplaceAll(ret, "\\", "\\\\")
		if strings.ContainsAny(ret, " \t`'") {
			ret = fmt.Sprintf(`"%s"`, ret)
		}
		return ret
	}

	// <basedir>/servers/command.txt
	cmdPath := filepath.Join(as.Directory, "servers", "command.txt")
	f, err := os.OpenFile(cmdPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Printf("Failed to open command file to server %v: %v", cmdPath, err)
		return
	}
	defer f.Close()

	// Quote the arguments to command if they're strings
	quoted := []interface{}{}
	for _, arg := range args {
		item := arg
		switch x := item.(type) {
		case string:
			item = quote(x)
		}
		quoted = append(quoted, item)
	}

	msg := fmt.Sprintf("%d,console,%s\n", as.Port, fmt.Sprintf(cmd, quoted...))
	//log.Printf("sending command: [%s]", strconv.Quote(msg))
	_, err = f.WriteString(msg)
	if err != nil {
		log.Printf("Failed to send msg '%v' to server %v: %v", msg, cmdPath, err)
		return
	}
}

func (as *AltiServer) Whisper(playerid int, msg string, args ...interface{}) {
	player := as.Players[playerid]
	if player == nil {
		log.Printf("Error invalid player given by id: %d", playerid)
		return
	}
	as.SendCmd("serverWhisper %s %s", player.Nick, fmt.Sprintf(msg, args...))
}

func (as *AltiServer) Message(msg string, args ...interface{}) {
	as.SendCmd("serverMessage %s", fmt.Sprintf(msg, args...))
}

func (as *AltiServer) RandomMap() (randomMap string) {
	for randomMap == "" {
		maps := as.AltiConf.MapList.String
		if len(maps) == 0 {
			log.Printf("Error no maps listed in server %v config", as.Port)
			return ""
		}
		randomMap = maps[rand.Intn(len(maps))].Value

		// Don't pick the lobby map
		if randomMap == as.LobbyMap {
			randomMap = ""
		}
	}
	log.Printf("Got random map for server %v as %v", as.Port, randomMap)
	return randomMap
}
