package main

import (
	"database/sql"
	"fmt"
)

type Storable interface {
	RecordRound(string, string) error
	UpdatePlayer(*Player) error
	UpdatePlayerStats(*Player, string) error
	GetPlayer(*Player) (*Player, error)
	CreateTables() error
}

type SqlStore struct {
	db   *sql.DB
	Type string
	Auth string
}

func NewSqlStore(kind, auth string) *SqlStore {
	return &SqlStore{Type: kind, Auth: auth}
}

func (s *SqlStore) Init() error {
	if s.db == nil {
		db, err := sql.Open(s.Type, s.Auth)
		if err != nil {
			return err
		}
		s.db = db
	}
	return nil
}

func (s *SqlStore) CreateTables() error {
	err := s.Init()
	if err != nil {
		return err
	}

	_, err = s.db.Exec(`
CREATE TABLE IF NOT EXISTS players (
	vapor_id text primary key,
	nickname text not null,
	wins integer default 0,
	losses integer default 0
);
CREATE TABLE IF NOT EXISTS players_stats (
	timestamp datetime default current_timestamp,
	vapor_id text not null,
	mode text not null ,
	wins integer,
	losses integer,
	rating real,

	PRIMARY key(vapor_id, mode)
);
CREATE TABLE IF NOT EXISTS rounds (
	timestamp datetime default current_timestamp,
	round_id integer primary key,
	mode text not null,
	stats text not null
);
`)
	return err
}

func (s *SqlStore) RecordRound(mode, roundStats string) error {
	if roundStats == "" {
		return fmt.Errorf("missing/empty roundStats not recording round")
	}
	if mode == "" {
		return fmt.Errorf("missing/empty game mode type for roundStats: %v", roundStats)
	}
	_, err := s.db.Exec(
		`INSERT INTO rounds (mode, stats) values (?, ?)`,
		mode,
		roundStats,
	)
	return err
}

// UpdatePlayerStats will replace stats in the database with the stats in
// the player object passed in
func (s *SqlStore) UpdatePlayerStats(player *Player, mode string) error {
	err := s.UpdatePlayer(player)
	if err != nil {
		return err
	}

	_, err = s.db.Exec(
		`REPLACE INTO players_stats (vapor_id, mode, wins, losses, rating) values (?, ?, ?, ?, ?)`,
		player.VaporId,
		mode,
		player.Wins,
		player.Losses,
		player.Rating,
	)
	return err
}

func (s *SqlStore) UpdatePlayer(player *Player) error {
	_, err := s.db.Exec(
		`REPLACE INTO players (vapor_id, nickname, wins, losses) values (?, ?, ?, ?)`,
		player.VaporId,
		player.Nick,
		player.Wins,
		player.Losses,
	)
	return err
}

func (s *SqlStore) GetPlayer(player *Player) (*Player, error) {
	rows, err := s.db.Query(
		`select * from players where vapor_id = ? or nickname = ?`,
		player.VaporId,
		player.Nick,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	/* Copy the existing player into the return */
	dbplayer := *player

	for rows.Next() {
		err = rows.Scan(&dbplayer.VaporId, &dbplayer.Nick, &dbplayer.Wins, &dbplayer.Losses)
		if err != nil {
			return nil, err
		}
		break
	}
	rows.Close()

	/* Load per-mode stats */
	stats, err := s.db.Query(
		`select mode, wins, losses, rating from players_stats where vapor_id = ?`,
		player.VaporId,
	)
	if err != nil {
		return nil, err
	}
	defer stats.Close()

	dbplayer.Stats = map[string]*PlayerStats{}
	for stats.Next() {
		wins := dbplayer.Wins
		losses := dbplayer.Losses
		rating := float64(0)
		mode := ""
		err = stats.Scan(&mode, &wins, &losses, &rating)
		if err != nil {
			return nil, err
		}

		dbplayer.Stats[mode] = &PlayerStats{Wins: wins, Losses: losses, Rating: rating}
	}

	return &dbplayer, nil
}
