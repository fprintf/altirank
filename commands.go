package main

import (
	"log"
	"time"
)

type Votes struct {
	Player map[string]int
}

// Global command hookshooks
var (
	CommandHooks map[string]func(*AltiEvent, *AltiServer) error
	ActiveVotes  map[int]*Votes
)

func init() {
	CommandHooks = map[string]func(*AltiEvent, *AltiServer) error{
		"y":     CmdVoteYes,
		"stats": CmdStats,
	}
	ActiveVotes = map[int]*Votes{}
}

func CmdVoteYes(e *AltiEvent, server *AltiServer) error {
	if server.Map != server.LobbyMap {
		log.Printf("Tried to vote while tournament in progress")
		return nil
	}
	vaporId := e.GetString("source")

	// Spectators can't vote
	player := server.Players[e.Player]
	if player == nil {
		return nil
	}
	if player.Team == -1 {
		server.Whisper(e.Player, "Spectators can't vote!")
		return nil
	}

	total := server.TotalPlayersActive()
	teamsize := server.TeamSize
	if server.TeamType == "balance" {
		teamsize *= 2
	}
	if total < teamsize {
		server.Message("Need %v more players to start a match", teamsize-total)
		return nil
	}

	votes := ActiveVotes[server.Port]
	if votes != nil {
		// Active vote happening, check to see if this user voted already
		vote := votes.Player[vaporId]
		if vote != 0 {
			// User has already voted, 0 is the default, -1 for no, 1 for yes
			server.Whisper(e.Player, "You already voted!")
			return nil
		}
	} else {
		votes = &Votes{Player: map[string]int{}}
		server.Message("Vote started for ranked play with %v players", total)
		server.Message("Type /y to vote yes. 75%% votes needed")
		ActiveVotes[server.Port] = votes
		go func() {
			time.Sleep(30 * time.Second)
			if ActiveVotes[server.Port] != nil {
				server.Message("Vote expired after 30 seconds")
				delete(ActiveVotes, server.Port)
			}
		}()
	}

	// you can only vote /y or nothing basically (by not voting at all)
	// so the only possible value if they did run /y is 1 for yes
	votes.Player[vaporId] = 1
	server.Whisper(e.GetInt("player"), "You voted Yes")

	// Count how many players have voted so far
	voted := 0
	for _, player := range server.Players {
		if votes.Player[player.VaporId] != 0 {
			voted++
		}
	}

	if result := float64(voted) / float64(total); result > .75 && ActiveVotes[server.Port] != nil {
		delete(ActiveVotes, server.Port)

		randomMap := server.RandomMap()
		delay := 5 * time.Second
		server.Message("%.2f%% players voted yes -- starting ranked play in %v on map [%v]",
			result*100,
			delay,
			randomMap,
		)
		server.SendCmd("startTournament")
		time.Sleep(delay)
		server.SendCmd("changeMap %s", randomMap)
	}
	return nil
}

func CmdStats(e *AltiEvent, server *AltiServer) error {
	vaporId := e.GetString("source")
	player, err := Store.GetPlayer(&Player{VaporId: vaporId})
	if err != nil {
		log.Printf("Error retrieving stats for %v: %v", vaporId, err)
		server.Whisper(e.Player, "Error retrieving your stats, please contact the admin.")
		return nil
	}

	if len(player.Stats) == 0 {
		server.Whisper(e.Player, "You will be given stats after you play a game.")
		return nil
	}

	for mode, stats := range player.Stats {
		player.Wins = stats.Wins
		player.Losses = stats.Losses

		server.Whisper(e.Player, "Player %s win loss ratio in [%s] is %02d-%02d (%.02f:1) rating %.2f",
			player.Nick,
			mode,
			player.Wins,
			player.Losses,
			player.Ratio(),
			player.Stats[mode].Rating,
		)
	}
	return nil
}
