package main

import (
	"fmt"
	"testing"
)

func TestMain(t *testing.T) {
	conf := &AltitudeLauncherConfig{}

	err := conf.LoadFile("./server1/servers/launcher_config.xml")
	if err != nil {
		t.Errorf("failed to load launcher_config: %#v", err)
	}
	fmt.Printf("%v", conf)
}
