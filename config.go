package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
)

// Config contains the config for the altirank server
// StorePath - filepath/uri of the backend storage (sqlite3 file, etc.)
// StoreType - the backend storage type (sqlite3, pg (postgresql), mysql, etc.)
// Servers - a map of servers by port integer defining configuration for each server
//    Name - name of server
//    Mode - mode of server, normal, coop, ranked
//    TeamType - balance, left, right, normal (none)
//    TeamSize - max team size relative to the type (e.g: balance with size 4 would eq 4v4 teams)
// TeamType - global type used if no per-server config exists
// TeamSize - global team size if no per-server config exists
type Config struct {
	StorePath      string
	StoreType      string
	Servers        map[int]*ConfigServer
	TeamType       string
	TeamSize       int
	RatingDefault  float64
	RatingMinGames int
}

type ConfigServer struct {
	Name      string
	Mode      string
	TeamType  string
	TeamSize  int
	Directory string
	LobbyMap  string
	AltiConf  *AltitudeServerConfig
}

func (c *Config) LoadFile(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	data, err := ioutil.ReadAll(f)
	tmp := &Config{}
	err = json.Unmarshal(data, tmp)
	if err != nil {
		return err
	}
	*c = *tmp
	return nil
}

func (c *Config) LoadFlags() error {
	return nil
}

func (c *Config) Load(file string) error {
	err := c.LoadFile(file)
	if err != nil {
		err = c.LoadFlags()
	}

	for port, server := range c.Servers {
		xmlFile := filepath.Join(server.Directory, "servers", "launcher_config.xml")
		altiConf := &AltitudeLauncherConfig{}
		err = altiConf.LoadFile(xmlFile)
		if err != nil {
			return err
		}
		server.AltiConf = &AltitudeServerConfig{}
		for _, serverConf := range altiConf.Servers.AltitudeServerConfig {
			confPort, _ := strconv.Atoi(serverConf.Port)
			if confPort == port {
				*server.AltiConf = serverConf
				break
			}
		}
	}

	return err
}
