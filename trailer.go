package main

import (
	"context"
	"fmt"
	"log"
	"os/exec"

	"github.com/nxadm/tail"
)

type Trailer struct {
	LogFile        string
	ServerLauncher string
	ProcessFunc    func(trailer *Trailer, line string) error
}

func (t *Trailer) Run(ctx context.Context) {
	cmd := &exec.Cmd{}
	// Run the server command (e.g: <basedir>/server_launcher)
	if t.ServerLauncher != "" {
		cmd = exec.CommandContext(ctx, t.ServerLauncher)
		err := cmd.Start()
		if err != nil {
			log.Println(err)
			return
		}
	}

	file, err := tail.TailFile(t.LogFile, tail.Config{Follow: true})
	if err != nil {
		log.Fatal(fmt.Errorf("Error opening %v: %w", t.LogFile, err))
	}

	for {
		select {
		case line, ok := <-file.Lines:
			if !ok {
				return
			}
			err := t.ProcessFunc(t, line.Text)
			if err != nil {
				log.Printf("%+v", err)
			}
		case <-ctx.Done():
			// Wait for our command to complete now that our context has been cancelled
			err = cmd.Wait()
			if err != nil {
				log.Fatal(fmt.Errorf("Error on exit server_launcher: %v", err))
			}
			return
		}
	}
}
