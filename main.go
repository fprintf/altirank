package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"path/filepath"
	"sync"
	"time"

	_ "github.com/mattn/go-sqlite3" // Add sqlite3 driver
)

/* Globals */
var (
	AltiServers AltiServerMap = AltiServerMap{RWMutex: &sync.RWMutex{}, Servers: map[int]*AltiServer{}}
	Store       Storable
	Conf        *Config
)

func main() {
	// Seed random number generation for use elsewhere
	rand.Seed(time.Now().UTC().UnixNano())

	Conf = &Config{}
	err := Conf.Load("altirank.conf")
	if err != nil {
		log.Fatal(err)
	}
	// Debugging information
	for i, ac := range Conf.Servers {
		log.Printf("server #%02d named %s teamSize: %d teamType: %s maps: %+v", i, ac.AltiConf.ServerName, ac.TeamSize, ac.TeamType, ac.AltiConf.MapList)
	}
	log.Printf("Got config: %#v", Conf)

	/* Load our Storage backend */
	Store = NewSqlStore(Conf.StoreType, Conf.StorePath)
	err = Store.CreateTables()
	if err != nil {
		log.Fatal(fmt.Sprintf("Error creating tables for storage: %+v", err))
	}

	/* Launch log readers for each log file */
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logfileTrailed := map[string]bool{}
	wg := &sync.WaitGroup{}
	for _, server := range Conf.Servers {
		server := server

		// Path is usually <directory>/servers/log.txt
		logfile := filepath.Join(server.Directory, "servers", "log.txt")

		// Don't trail the same logfile more than once
		if logfileTrailed[logfile] {
			continue
		}
		logfileTrailed[logfile] = true

		trailer := &Trailer{LogFile: logfile, ProcessFunc: func(trailer *Trailer, text string) error {
			// Uncomment to see the raw line printed for debugging..
			// log.Println(text)

			/* Process json */
			event, err := NewAltiEvent(trailer, text)
			if err != nil {
				return fmt.Errorf("failed to parse json: %v: %w", text, err)
			}

			return event.Route()
		}}

		wg.Add(1)
		go func(trailer *Trailer) {
			trailer.Run(ctx)
			wg.Done()
		}(trailer)
	}

	/* Wait for our readers to exit */
	wg.Wait()
}
