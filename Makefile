.PHONY: release


all: build test release

build: *.go go.mod go.sum
	go build

test: *.go go.mod go.sum
	go test -vet ""

release: *.go go.mod go.sum altirank.conf
	tar -czvf altirank-src.tar.gz $+
